/*
 * =====================================================================================
 *
 *       Filename:  solution1.cpp
 *
 *    Description:  for ctci question1.1
 *
 *        Version:  1.0
 *        Created:  08/19/2014 17:07:31
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <iostream>
#include <cstring>
using namespace std;

bool isUnique(string s){
  bool a[256];
  memset(a, 0, sizeof(a));
  int len = s.length();
  for (int i = 0; i < len; i++) {
    int v = (int)s[i];
    if (a[v]) return false;
    a[v] = true;
  }
  return true;
}

int main(){
  string s1 = "hello";
  string s2 = "bye";
  cout << s1 << " " << isUnique(s1) << " " << s2 << " " << isUnique(s2) << endl;
}
