#!/usr/local/bin/python3
# -*- coding: utf-8 -*-


def is_unique(str):
    charsTable = set()
    for c in str:
        if c in charsTable:
            return False
        charsTable.add(c)
    return True


test_str1 = "hello"
test_str2 = "abcdef"
ans1 = is_unique(test_str1)
ans2 = is_unique(test_str2)
print(ans1)
print(ans2)
