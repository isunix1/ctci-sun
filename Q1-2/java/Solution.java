import java.util.*;

public class Solution{
  public static void swap(char a, char b){
    char temp;
    temp = a;
    a = b;
    b = temp;
  }

  public static String reverse(String s){
    int n = s.length();
    int i;
    for (i=0; i<n/2; i++) {
      swap(s.charAt(i), s.charAt(n-i-1));
    }
    return s;
  }

  public static void main(String[] args){
    String str = "abcdef";
    Solution slt = new Solution();
    System.out.println("the original string is " + str + " and the reversed string is:\n" + slt.reverse(str));
  }
}
