/*
 * =====================================================================================
 *
 *       Filename:  Solution.cpp
 *
 *    Description:  for the Q1.2 in CTCI.
 *
 *        Version:  1.0
 *        Created:  08/20/2014 11:02:58
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */

#include <iostream>
#include <cstring>

using namespace std;

void reverse(char *str){
  char* end = str;
  char tmp;
  if(str){
    while(*end){
      ++end;
    }
    --end;
    //find the end of the string, since the last char is nul, we use --end to set one char back.
    while(str < end){
      tmp = *str;
      *str++ = *end;
      *end-- = tmp;
    }
  }
}

int main(){
  char s1[] = "123456789";
  char s2[] = "abcdefg";
  reverse(s1);
  cout<<s1<<endl;
  reverse(s2);
  cout<<s2<<endl;
}
