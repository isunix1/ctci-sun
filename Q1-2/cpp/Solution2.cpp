/*
 * =====================================================================================
 *
 *       Filename:  Solution2.cpp
 *
 *    Description:  for the Q1.2 in CTCI.
 *
 *        Version:  1.0
 *        Created:  08/20/2014 11:02:58
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */

#include <iostream>
#include <cstring>

using namespace std;


void swap(char &a, char &b){
  a = a^b;
  b = a^b;
  a = a^b;
}

void reverse(char *s){
  int n = strlen(s);
  int i;
  for (i = 0; i < n/2; i++) {
    swap(s[i], s[n-i-1]);
  }
}

int main(){
  char s1[] = "123456789";
  char s2[] = "abcdefg";
  reverse(s1);
  cout<<s1<<endl;
  reverse(s2);
  cout<<s2<<endl;
}
