#!/usr/local/bin/python3
# -*- coding: utf-8 -*-


def reverseStringRecursive(str):
    if str != "":
        return str[-1:] + reverseStringRecursive(str[:-1])
    else:
        return ""



str1 = "abcdef"
str2 = "123456"

print(reverseStringRecursive(str1))
print(reverseStringRecursive(str2))
